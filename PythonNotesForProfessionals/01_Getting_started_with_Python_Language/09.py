#!/usr/bin/env python3

# Section 1.9: Creating a module

# A module is an importable file containing definitions and statements.

# A module can be created by creating a `.py` file.

# [hello.py source]

# Functions in a module can be usede by importing the module.

# For modules that you have made, they will need to be in the same directory as the file that you are importing them into.
# (However, you can also put them into the Python lib directory with the pre-included modules, but should be avoided if possible.)

import hello
hello.say_hello()

# Modules can be imported by other modules.

# [greet.py source]

# A module can be stand-alone runnable script.

# [run_hello.py source]

# If the module is inside a directory and needs to be detected by Python, the directory should contain a file named `__init__.py`.