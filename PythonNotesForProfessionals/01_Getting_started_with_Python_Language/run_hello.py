#!/usr/bin/env python3

# A module can be stand-alone runnable script.

if __name__ == "__main__":
    from hello import say_hello
    say_hello()

# Run it!
# python run_hello.py