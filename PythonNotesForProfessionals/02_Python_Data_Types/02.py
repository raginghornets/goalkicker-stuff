#!/usr/bin/env python3

# Section 2.2: Set Data Types

# Sets are unordered collections of unique objects, there are two types of set:

# 1. Sets - They are mutable and new elements can be added once sets are defined

basket = {'apple', 'orange', 'apple', 'pear', 'orange', 'banana'}
print(basket) # duplicates will be removed
a = set('abracadabra')
print(a) # unique letters in a
a.add('z')
print(a)

# 2. Frozen Sets - They are immutable and new elements cannot be added after its defined.

b = frozenset('asdfagsa')
print(b)
cities = frozenset(["Frankfurt", "Basel", "Freiburg"])