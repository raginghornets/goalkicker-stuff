// Chapter 1: Getting started with C++

// Section 1.1: Hello World

// This program prints `Hello World!` to the standard output stream:

#include <iostream>

int main() {
  std::cout << "Hello, world!" << std::endl;
}

// Analysis
// Let's examine each part of this code in detail:
// - #include <iostream> is a preprocessor directive that includes the content of the standard C++ header file `iostream`

// `iostream` is a standard library header file that contains definitions of the
// standard input and output. These definitions are included in the `std` namesp
// ace, explained below.

// The standard input/output (I/O) streams provide ways for programs to get inpu
// t from and output to an external system -- usually the terminal.

// - `int main() { ... }` defines a new function named `main`. By convention, th
// e `main` function is called upon execution of the program. There must be only 
// one `main` function in a C++ program, and it must always return a number of t
// he `int` type.

// Here the `int` is what is called the function's return type. The value return
// ed by the `main` function is an exit code.

// By convention, a program exit code of 0 or `EXIT_SUCCESS` is interpreted as s
// uccess by a system that executes the program. Any other return code is associ
// ated with an error.

// If no `return` statement is present, the `main` function (and thus, the progr
// am itself) returns 0 by default. In this example, we don't need to explicitly
// `return 0;`.

// All other functions, except those that return the `void` type, must explicitl
// y return a value according to their return type, or else must not return at a
// ll

// - `std::cout << "Hello, world!" << std::endl;` prints "Hello, world!" to the 
//   standard output stream:
//    * `std` is a namespace, and `::` is the scope resolution operator that all
//      ows look-ups for objects by name within a namespace.

//      There are many namespaces. Here, we use `::` to show we want to user `co
//      ut` from the `std` namespace. For more information refer to Scop Resolut
//      ion Operator - Microsoft Documentation.

//    * `std::cout` is the standard output stream object, defined in `iostream`,
//      and it prints to the standard output (stdout).

//    * `<<` is, in this context, the stream insertion operator, so called becau
//      se it inserts an object into the stream object.

//      The standard library defines the `<<` operator to perform data insertion 
//      for certain data types into output streams, `stream << content` inserts i
//      nto the stream and returns the same, but updated stream. This allows stre
//      am insertions to be chained: `std::cout << "Foo" << "Bar";` prints "FooB
//      ar" to the console.

//    * "Hello, world!" is a character string literal, or a "text literal". The 
//      stream insertion operator for character string literals is defined in fi
//      le `iostream`

//    * `std::endl` is a special I/O stream manipulator object, also defined in 
//      file `iostream`. Inserting a manipulator into a stream changes the state
//      of the stream.

//      The stream manipulator `std::endl` does two things: first it inserts the
//      end-of-line character and then it flushes the stream buffer to force the
//      text to show up on the console. This ensures that the data inserted into
//      the stream actually appear on your console. (Stream data is usually stor
//      ed in a buffer and then "flushed" in batches unless you force a flush im
//      mediately.)

//      An alternate method that avoids the flush is:

//      std::cout << "Hello, world!\n";

//      where \n is the character escape sequence for the newline character.

//      * The semicolon (;) notifies the compiler that a statement has ended. Al
//      l C++ statements and class definitions require an ending/terminating sem
//      icolon.