#include <iostream>

class Widget {
  private:
    int x;
    int y;
    int z;
  public:
    void beep() {
      std::cout << "Beep!" << std::endl;
    }
};

int main(void) {
  Widget* p = new Widget();
  p->beep();
  std::cout << p << std::endl;
  delete p;
  p = nullptr;
  p->beep();
  std::cout << p << std::endl;
}
