#include <stdio.h>
#include <stdlib.h>

struct node {
  struct node *next;
  struct node *child;
  char* data;
};

void printtree_r(struct node *node, int depth) {
  int i;

  while (node) {
    if (node->child) {
      printf("Node Child Data: %s\n", node->child->data);
      for (i = 0; i < depth * 3; i++) {
        printf(" ");
      }
      printf("{\n");
      printtree_r(node->child, depth + 1);
      for (i = 0; i < depth * 3; i++) {
        printf(" ");
      }
      printf("{\n");
      for (i = 0; i < depth * 3; i++) {
        printf(" ");
      }
      printf("%s\n", node->data);
    }

    node = node->next;
  }
}

void printtree(struct node *root) {
  printtree_r(root, 0);
}

int main(void) {
  struct node* node1 = malloc(sizeof(struct node));
  struct node* node2 = malloc(sizeof(struct node));
  struct node* node3 = malloc(sizeof(struct node));
  node1->data = "Hello!";
  node2->data = "Hi!";
  node3->data = "Hey!";
  node1->child = node2;
  node1->next = node2;
  node2->next = node3;
  printf("%s", node1->data);
  printtree(node1);
  return 0;
}