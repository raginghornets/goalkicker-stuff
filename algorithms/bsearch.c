#include <stdio.h>

/**
 * Performs a binary search on a sorted array of integers
 */
int bSearch(int arr[], int size, int item) {
  int low = 0;
  int mid;
  int high = size - 1;

  while (low <= high) {
    mid = low + (high - low) / 2;
    if (arr[mid] == item) {
      return mid;
    } else if (arr[mid] < item) {
      low = mid + 1;
    } else {
      high = mid + 1;
    }
  }
  return -1;
}

int main(void) {
  int arr[] = {1, 2, 3, 4, 5, 6};
  int size = 6;
  for (int i = 0; i < size; i++) {
    printf("%d ", arr[i]);
  }
  printf("\n");
  printf("Found 6: %d", bSearch(arr, size, 6));
}