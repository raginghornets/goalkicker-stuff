# The New-Object cmdlet is used to create an object in PowerShell
$var = New-Object System.DateTime

# calling constructor with parameters
# $sr = New-Object System.IO.StreamReader -ArgumentList "file path"

# In many instances, a new object will be created in order to export data or pass it to another commandlet. This can be done like so:
$newObject = New-Object -TypeName PSObject -Property @{
  ComputerName = "SERVER1"
  Role = "Interface"
  Environment = "Production"
}

# There are many ways of creating an object. The following method is probably the shortest and fastest way to create a PSCustomObject:
$newObject = [PSCustomObject]@{
  ComputerName = "SERVER1"
  Role = "Interface"
  Environment = "Production"
}

# In case you already have an object, but only need one or two extra properties, you can simply add that property by using Select Object:
Get-ChildItem | Select-Object FullName, Name,
  @{Name="DateTime"; Expression={Get-Date}},
  @{Name="PropertyName"; Expression={"CustomValue"}}
