# Write-Host only writes to stdout
Write-Host "Write-Host says: Hello, world!"

# Write-Output writes to stdout and output [success] stream allowing for rediction
# Write-Output Aliases: Echo, Write
Write-Output "Write-Output says: Hello, world!"

Get-ChildItem # Aliases: dir, ls

# Set temporary alias "ping" for command "Test-NetConnection"
Set-Alias -Name ping -Value Test-NetConnection
ping
